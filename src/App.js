import React, { useState, useEffect, lazy, Suspense } from "react";
import "./App.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import axios from "axios";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Button from "./components/button/Button";
import IncorrectComponent from "./components/IncorrectComponent";
import UntaggedComponent from "./components/UntaggedComponent";
import DisabledComponent from "./components/DisabledComponent";
import HighMarginStatus from "./components/HighMarginRecipe/HighMarginStatus";
import LowMarginStatus from "./components/LowMarginRecipe/LowMarginStatus";
import FluctingStatus from "./components/TopFluctuatingRecipe/FluctingStatus";

const DataTable = lazy(() => import("./components/dataTable/DataTable.js"));

export const RecipeContext = React.createContext();

function App() {
  const [details, setDetails] = useState([]);

  const fetchDetails = async () => {
    const { data } = await axios.get(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?format=json"
    );
    console.log(data.results);

    const details = data.results;
    setDetails(details);
  };

  useEffect(() => {
    fetchDetails();
  }, []);

  return (
    <Router>
      <RecipeContext.Provider value={details}>
        <div className="hide-scrollbar">
          <header className="header-progress">
            {/* TODO PROGRESS STATUS BAR */}
            <HighMarginStatus />
            <LowMarginStatus />
            <FluctingStatus />
          </header>

          <div className="main-data-button">
            <Button />
            <Suspense
              fallback={
                <div className="display-component">
                  <Loader
                    type="BallTriangle"
                    color="#00BFFF"
                    height={100}
                    width={100}
                  />
                </div>
              }
            >
              <Route exact path="/" component={DataTable} />
            </Suspense>
            <Route exact path="/incorrect" component={IncorrectComponent} />
            <Route exact path="/untagged" component={UntaggedComponent} />
            <Route exact path="/disabled" component={DisabledComponent} />
          </div>
        </div>
      </RecipeContext.Provider>
    </Router>
  );
}

export default App;
