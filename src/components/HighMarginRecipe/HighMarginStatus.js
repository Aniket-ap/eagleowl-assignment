import React from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import "./HighMarginStatus.css";

const HighMarginStatus = () => {
  const percentage = 80;
  return (
    <div className="high-margin-main">
      <h6>High Margin Recipes</h6>
      <table>
        <tr>
          <td>Ambur Biriyani</td>
          <td>Panner Tikka Masala</td>
          <td>Palak Panner Butter Masala</td>
        </tr>
        <tr>
          <td>
            <CircularProgressbar
              value={percentage}
              text={`${percentage}%`}
              strokeWidth={5}
              styles={buildStyles({
                textColor: "turquoise",
                pathColor: "turquoise",
              })}
            />
          </td>
          <td>
            <CircularProgressbar
              value={percentage}
              text={`${percentage}%`}
              strokeWidth={5}
              styles={buildStyles({
                textColor: "turquoise",
                pathColor: "turquoise",
              })}
            />
          </td>
          <td>
            <CircularProgressbar
              value={percentage}
              text={`${percentage}%`}
              strokeWidth={5}
              styles={buildStyles({
                textColor: "turquoise",
                pathColor: "turquoise",
              })}
            />
          </td>
        </tr>
      </table>
    </div>
  );
};

export default HighMarginStatus;
