import React from "react";

const DisabledComponent = () => {
  return (
    <div>
      <h1 className="display-component">Disabled Component</h1>
      <h4 className="display-component">
        :( I am sorry, I dont know what to display here...!!!
      </h4>
    </div>
  );
};

export default DisabledComponent;
