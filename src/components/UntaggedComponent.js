import React from "react";

const UntaggedComponent = () => {
  return (
    <div>
      <h1 className="display-component">Untagged Component</h1>
      <h4 className="display-component">
        :( I am sorry, I dont know what to display here...!!!
      </h4>
    </div>
  );
};

export default UntaggedComponent;
