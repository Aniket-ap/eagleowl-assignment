import React from "react";

const IncorrectComponent = () => {
  return (
    <div>
      <h1 className="display-component">Incorrect Component</h1>
      <h4 className="display-component">
        :( I am sorry, I dont know what to display here...!!!
      </h4>
    </div>
  );
};

export default IncorrectComponent;
