import React from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import "./FluctingStatus.css";

const FluctingStatus = () => {
  return (
    <div className="fluctatuing-main">
      <h6>Top Fluctuating Recipes</h6>
      <table>
        <tr>
          <td>Ambur Biriyani</td>
          <td>Panner Tikka Masala</td>
          <td>Palak Panner Butter Masala</td>
        </tr>
        <tr id="status-rate">
          <td>5% ↑</td>
          <td>3% ↓</td>
          <td>3% ↓</td>
        </tr>
      </table>
    </div>
  );
};

export default FluctingStatus;
