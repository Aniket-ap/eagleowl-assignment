import React from "react";
import { Link } from "react-router-dom";
import "./Button.css";

const Button = () => {
  // const isActive = true;

  return (
    <div className="buttons-nav">
      <button className="botton-nav">
        <Link to="/">ALL RECIPE(S)</Link>
      </button>
      <button className="botton-nav">
        <Link to="/incorrect">INCORRECT</Link>
      </button>
      <button className="botton-nav">
        <Link to="/untagged">UNTAGGED</Link>
      </button>
      <button className="botton-nav">
        <Link to="/disabled">DISABLED</Link>
      </button>
    </div>
  );
};

export default Button;
