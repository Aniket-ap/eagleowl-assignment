import React, { useContext, useState } from "react";
import "./DataTable.css";

import { RecipeContext } from "../../App";
import TableTags from "../tableTags/TableTags";

const DataTable = () => {
  const [selectAll, setSelectAll] = useState(false);
  const details = useContext(RecipeContext);
  // console.log(details);
  return (
    <div className="main-table-div">
      <table className="recipes">
        <tr>
          <th>
            <input
              type="checkbox"
              id="selectAll"
              value={selectAll}
              onClick={() => setSelectAll(!selectAll)}
            />
          </th>
          <th>NAME</th>
          <th>LAST UPDATED</th>
          <th>COGS%</th>
          <th>COST PRICE</th>
          <th>SALE PRICE</th>
          <th>GROSS MARGIN</th>
          <th>TAGS / ACTIONS</th>
        </tr>
        {details.map((recipe) => {
          return (
            <tr key={recipe.id}>
              <td>
                <input type="checkbox" checked={selectAll} />
              </td>
              <td>{recipe.name}</td>
              <td>{recipe.last_updated.date.substr(0, 10)}</td>
              <td>{recipe.cogs}%</td>
              <td>{recipe.cost_price.toFixed(2)}</td>
              <td>{recipe.sale_price.toFixed(2)}</td>
              <td>{recipe.gross_margin.toFixed(0)}%</td>
              <td>
                <TableTags />
                <TableTags />
              </td>
            </tr>
          );
        })}
      </table>
    </div>
  );
};

export default DataTable;
