import React from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import "./LowMarginStatus.css";

const LowMarginStatus = () => {
  return (
    <div className="low-margin-main">
      <h6>Low Margin Recipes</h6>
      <table>
        <tr>
          <td>Ambur Biriyani</td>
          <td>Panner Tikka Masala</td>
          <td>Palak Panner Butter Masala</td>
        </tr>
        <tr>
          <td>
            <CircularProgressbar
              value="48"
              text="48%"
              strokeWidth={5}
              styles={buildStyles({
                textColor: "red",
                pathColor: "red",
              })}
            />
          </td>
          <td>
            <CircularProgressbar
              value="5"
              text="5%"
              strokeWidth={5}
              styles={buildStyles({
                textColor: "red",
                pathColor: "red",
              })}
            />
          </td>
          <td>
            <CircularProgressbar
              value="15"
              text="15%"
              strokeWidth={5}
              styles={buildStyles({
                textColor: "red",
                pathColor: "red",
              })}
            />
          </td>
        </tr>
      </table>
    </div>
  );
};

export default LowMarginStatus;
